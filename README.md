# frontend-poc

This is a proof of concept for a Cybersecurity vulnerability management software. Simple frontend built in Vue3, TypeScript, Vuetify, and Pinia. This is mostly to demo full lifecycle flow for a frontend application. A big chunk of the UI work is from a template and needs some proper TLC to be up to snuff with standards (ie, not just using "string" for types when an Enum is the correct way). 

Deployed [here](http://cyberrity.s3-website-us-west-1.amazonaws.com/)

### Quick Dev Setup

```
npm install
npm run start
```

## Features

1. TypeScript
2. Eslint/Prettier formatting
3. Gitlab CI/CD with automatic deploys and mandatory tests passing
4. Vuetify
5. Pre-commit test validation
6. Hosted in AWS S3

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

Enable Volar Takeover mode for better TypeScript support [here](https://vuejs.org/guide/typescript/overview.html#volar-takeover-mode)

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

```sh
npm run test:e2e:dev
```

This runs the end-to-end tests against the Vite development server.
It is much faster than the production build.

But it's still recommended to test the production build with `test:e2e` before deploying (e.g. in CI environments):

```sh
npm run build
npm run test:e2e
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

import '@/scss/style.scss'

import { createPinia } from 'pinia'
import { createApp } from 'vue'
import PerfectScrollbar from 'vue3-perfect-scrollbar'
import VueApexCharts from 'vue3-apexcharts'

import App from './App.vue'
import { router } from './router'
import vuetify from './plugins/vuetify'

const app = createApp(App)
app.use(router)
app.use(PerfectScrollbar)
app.use(VueApexCharts)
app.use(createPinia())
app.use(vuetify).mount('#app')

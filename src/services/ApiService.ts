import proimg1 from '@/assets/images/articles/article1.webp'
import proimg2 from '@/assets/images/articles/article2.jpg'
import proimg3 from '@/assets/images/articles/article3.jpg'
import proimg4 from '@/assets/images/articles/article4.webp'
import type { newsItems } from '@/types/dashboard'
const news: newsItems[] = [
  {
    title: "IT Security is broken - and it's my fault",
    link: 'https://www.scmagazine.com/perspective/it-security-is-broken-and-its-my-fault',
    photo: proimg1,
    salesPrice: 375,
    price: 285,
    rating: 4
  },
  {
    title: 'S vs. CRWD: Which Cybersecurity Stock is the Better Buy?',
    link: 'https://www.tipranks.com/news/article/s-vs-crwd-which-cybersecurity-stock-is-the-better-buy',
    photo: proimg2,
    salesPrice: 650,
    price: 900,
    rating: 5
  },
  {
    title: 'Security Think Tank: 2024 is the year we bridge the cyber divide',
    link: 'https://www.computerweekly.com/opinion/Security-Think-Tank-2024-is-the-year-we-bridge-the-cyber-divide',
    photo: proimg3,
    salesPrice: 150,
    price: 200,
    rating: 3
  },
  {
    title: 'Malicious PyPI Packages Slip WhiteSnake InfoStealer Malware onto Windows Machines',
    link: 'https://thehackernews.com/2024/01/malicious-pypi-packages-slip-whitesnake.html',
    photo: proimg4,
    salesPrice: 285,
    price: 345,
    rating: 2
  }
]

class ApiService {
  public getCyberNews = async () => {
    try {
      return news
    } catch (error) {
      console.error(error)
      return []
    }
  }
}

export default new ApiService()

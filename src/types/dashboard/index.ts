/*Recent Transaction*/
type Incident = {
  id: number
  title: Date
  subtitle: string
  textcolor: string
  link?: string
  url?: string
}

/*product performance*/
type vulnerabilitiesType = {
  id: number
  name: string
  title: string
  category: 'Frontend' | 'Backend' | 'DevOps' | 'Database'
  status: 'Low' | 'Medium' | 'High'
  statuscolor: 'error' | 'secondary' | 'warning'
  estimatedTtr: string
}

/*Products card types*/
type newsItems = {
  title: string
  link: string
  photo: string
  salesPrice: number
  price: number
  rating: number
}

export type { Incident, vulnerabilitiesType, newsItems }

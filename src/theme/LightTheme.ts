import type { ThemeTypes } from '@/types/themeTypes/ThemeType'

const LightTheme: ThemeTypes = {
  name: 'LightTheme',
  dark: false,
  variables: {
    'border-color': '#eeeeee',
    'carousel-control-size': 10
  },
  colors: {
    primary: '#16141B',
    secondary: '#687993',
    info: '#539BFF',
    success: '#007E33',
    accent: '#FFAB91',
    warning: '#ffbb33',
    error: '#CC0000',
    muted: '#5a6a85',
    lightprimary: '#EAEFF4',
    lightsecondary: '#E8F7FF',
    lightsuccess: '#E6FFFA',
    lighterror: '#FDEDE8',
    lightwarning: '#FEF5E5',
    textPrimary: '#2A3547',
    textSecondary: '#2A3547',
    borderColor: '#e5eaef',
    inputBorder: '#000',
    containerBg: '#ffffff',
    hoverColor: '#f6f9fc',
    surface: '#F1F3F5',
    'on-surface-variant': '#DBD5DD',
    cardBackground: '#FAFAF8',
    grey100: '#F2F6FA',
    grey200: '#EAEFF4',
    background: '#E1E1E1',
    chartSeries: '#49BEFF'
  }
}
export { LightTheme }

import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import Dashboard from '../dashboard/index.vue'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const vuetify = createVuetify({
  components,
  directives
})
describe('Just some test', () => {
  it('renders properly', () => {
    const wrapper = mount(Dashboard, {
      global: {
        components: { Dashboard },
        plugins: [vuetify]
      }
    })
    expect(wrapper.text()).toContain('Outstanding Vulnerabilities')
  })
})

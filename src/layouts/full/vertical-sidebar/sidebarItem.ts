import {
  IconAperture,
  IconCopy,
  IconLayoutDashboard,
  IconMoodHappy,
  type SVGProps
} from '@tabler/icons-vue'
import type { FunctionalComponent } from 'vue'

export interface MenuItem {
  header?: string
  title?: string
  icon?: FunctionalComponent<SVGProps>
  to?: string
  chip?: string
  chipColor?: string
  chipVariant?: NonNullable<'flat' | 'text' | 'elevated' | 'tonal' | 'outlined' | 'plain'>
  chipIcon?: string
  children?: MenuItem[]
  disabled?: boolean
  type?: string
  subCaption?: string
}

const sidebarItems: MenuItem[] = [
  { header: 'Home' },
  {
    title: 'Dashboard',
    icon: IconLayoutDashboard,
    to: '/'
  },
  { header: 'utilities' },
  {
    title: 'Typography',
    icon: IconCopy,
    to: '/ui/typography'
  },
  {
    title: 'Icons',
    icon: IconMoodHappy,
    to: '/icons'
  },
  {
    title: 'Sample Page',
    icon: IconAperture,
    to: '/sample-page'
  }
]

export default sidebarItems
